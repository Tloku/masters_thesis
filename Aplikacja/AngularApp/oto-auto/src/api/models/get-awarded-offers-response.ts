import { OfferCardComponentModel } from "src/store/offer/offer-card-component.model";

export interface GetAwardedOffersResponse {
    response: OfferCardComponentModel[]
}