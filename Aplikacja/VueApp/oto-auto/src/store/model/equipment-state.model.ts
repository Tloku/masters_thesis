import { EquipmentType } from "./create-offer-form.model";

export interface EquipmentStateModel{
    equipmentTypes: EquipmentType[] 
}